

"use strict"



//1

//створюємо всі елементи
const elFooter = document.querySelector('footer')
const pForEx1 = elFooter.querySelector('p');
const aForEx1 = document.createElement('a');

//Задаємо параметри
aForEx1.textContent = 'Learn More';
aForEx1.href = '#';

//Добаємо aForEx1 після pForEx1 з умовою nextSibling (він "обертає" метод insertBefore) 
elFooter.insertBefore(aForEx1, pForEx1.nextSibling);



//2


//Відразу знайдемо існуючі елементи

const elMain = document.createElement('main');
const secFeatures = document.querySelector('.features');

// Створюємо новий елемент

const elSelect = document.createElement('select');

//Додаємо <select> перед секцією "Features" в майні

elMain.insertBefore(elSelect, secFeatures);



//Задамо ідентифікатор для elSelect
elSelect.id = 'rating';

// Створюємо елементи зі значеннями та текстом
const option1 = document.createElement('option');
option1.value = '1';
option1.textContent = '1 Star';

// Додаємо елементи до списку
elSelect.appendChild(option1);

//Провести те саме з усіма іншими елементами
// const option2 = document.createElement('option');
// option1.value = '2';
// option1.textContent = '2 Stars';
// selectElement.appendChild(option2);

//Або можна виконати в циклі

for (let i = 2; i <= 4; i++) {
    const option = document.createElement('option');
    option.value = i.toString();
    option.textContent = `${i} Stars`;
    elSelect.appendChild(option);
  }

//Або взагалі створити функцію аргументом якої буде кількість option

function ex2Func(quan) {
    for (let i = 2; i <= quan; i++) {
        const option = document.createElement('option');
        option.value = i.toString();
        //Доведеться виконати валідацію для більш коректного написання
        option.textContent = i === 1 ? i + ' Star' : i + ' Stars';
        elSelect.appendChild(option);
      }
}
